# qa-images

Building ST images for quality and assurance, i.e. testing all aspects of ST.

A set of generic **unsigned** OS images are being published in the
GitLab [package registry][]. Tagging this repository triggers new
builds to be published there.

**Signed** OS images from a selection of CI/CD jobs built from the
main branch in this repository are published in the [Glasklar ST archive for QA][].
OS images are signed using keys signed by the [Glasklar QA ST signing root][].

[package registry]: https://git.glasklar.is/system-transparency/project/qa-images/-/packages
[Glasklar ST archive for QA]: https://st.glasklar.is/st/qa/
[Glasklar QA ST signing root]: https://git.glasklar.is/glasklar/trust/glasklar-ca/-/tree/main/QA-CA/ST-signing

## stboot images

- [x] Unsigned stboot UKI
- [x] Unsigned stboot ISO (unsigned stboot UKI in an ISO)
- [ ] Signed stboot UKI

- Trust policy ST root: [Glasklar QA ST signing root][]
- UEFI Secure Boot keys: TODO:db-key (user will have to sign the db key with their KEK, FIXME:doc)

[Glasklar QA ST signing root]: https://git.glasklar.is/glasklar/trust/glasklar-ca/-/tree/main/QA-CA/ST-signing

## ST OS images

- [x] Basic test image, Debian
  - ST-signature: Glasklar QA ST signing key
  - OS: Debian Bookworm
- [ ] Basic test image, Debian next
  - ST-signature: Glasklar QA ST signing key
  - OS: Debian Trixie

## For convenience

- [x] stboot-kernel.gz
- [ ] stprov-kernel.gz

## The GitLab Registry

Use https://git.glasklar.is/system-transparency/project/qa-images/-/packages/
for a user friendly overview of published (unsigned) OS images.

Use `"https://git.glasklar.is/api/v4/projects/110/packages/generic/$JOB/$TAG/$FILENAME"`
for a machine friendly download URL.
Example for `v2` of the job `qa-stboot-kernel` which publishes the file `qa-stboot-kernel-amd64.vmlinuz`:

    https://git.glasklar.is/api/v4/projects/110/packages/generic/qa-stboot-kernel/v2/qa-stboot-kernel-amd64.vmlinuz

## Checksum files

- [ ] SHA512SUMS -- SHA-512 checksums

## Detached signatures

- [ ] <FILENAME>.sshsig -- SSH signature
- [ ] <FILENAME>.pgpsig -- PGP signature

- SSH signatures: [linus:YK5C-31][]
- PGP signatures: [rsa4096/1E8BF34923291265][]

[linus:YK5C-31]: https://git.glasklar.is/linus.keys
[rsa4096/1E8BF34923291265]: https://git.glasklar.is/linus.gpg


### A word on detached signatures

What exactly is it that these signatures are meant to assert?

When someone is signing a source tree it is intuitively easy to
understand this as "I'm signing the source code I've written, plus
some source code I've not written but for which I've verified
signatures made by others." The "others" here opens up a can of worms
and one can only hope that the signer of the source made their due
diligence.

When someone is signing a binary it should be understood as something
like "I'm signing the binary code I've had computers compile from
source code for which I've verified signatures made by others." The
"others" comes with the same set of problems as in the signing source
case, only to a potentially greater degree in a large project with
many sources of source code. The "computers compile" is a nightmare,
until one has full reproducibility and build info from a number of
(collectively) trusted rebuilders.

TODO: State what a detached signature over an executable means by
describing the process by which the signature and executable are made.
