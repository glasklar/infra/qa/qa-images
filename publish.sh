#! /bin/bash
set -eu

# Publish to the GitLab package registry.

source="$1"; shift
bn=$(basename $source)
dn=$(dirname $source)

for suffix in $@; do
    fn="${bn}${suffix}"
    URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_JOB_NAME_SLUG}/${CI_COMMIT_TAG}/${fn}"

    curl --silent \
         --header "JOB-TOKEN: $CI_JOB_TOKEN" \
         --upload-file "${dn}/${fn}" \
         "$URL"
done
